<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Closers.com</title>
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            @include('common.header')
            <b-container class="page-wrapper">
                <b-row>
                    <b-col class="d-flex flex-column align-items-center col-12 col-lg-5">
                        <h2>Where Closers and Influencers Get Connected</h2>
                        <p class="mr-lg-4 pr-lg-4">Get early access to match with the top High-Ticket Closers&trade;</p>
                        <div class="button-wrapper d-flex flex-row justify-content-between mt-4 pt-3">
                            <b-button variant="primary" size="lg" href="{{ url('#') }}">Request to join</b-button>
                            <b-button variant="secondary" size="lg" href="{{ url('#') }}">Post an opportunity</b-button>
                        </div>
                    </b-col>
                    <b-col class="col-7 d-none mt-5 mt-lg-0 d-lg-flex justify-content-end">
                        <b-img src="{{ url('/images/illustrations/00.png') }}" fluid></b-img>
                    </b-col>
                </b-row>
                <b-row>
                    <b-col class="col-6 d-none d-lg-flex justify-content-start">
                        <slide-animation
                            :data-step="1"
                            src="{{ url('/images/illustrations/01.png') }}"
                            direction="left"
                        ></slide-animation>
                    </b-col>
                    <b-col class="d-flex flex-column align-items-start col-12 col-lg-6">
                        <b-img src="{{ url('/images/icons/icon-1.jpg') }}" left blank-src class="mt-5 pt-2"></b-img>
                        <h3>Find Closers That Fit Your Offer</h3>
                        <p class="mr-lg-5 pr-lg-4">We have High-Ticket Closers&trade; who have closed High-Ticket Offers for every industry you can imagine. From health, to education, to business opportunities, to real estate, and everything in between.</p>
                        <p class="mr-lg-5 pr-lg-4">We have a High-Ticket Closer&trade; for any challenge.</p>
                        <div class="button-wrapper d-flex flex-row justify-content-between mt-4 pt-3">
                            <b-button variant="primary" size="lg" href="{{ url('#') }}">Request to join</b-button>
                        </div>
                    </b-col>
                </b-row>
                <b-row>
                    <b-col class="d-flex flex-column align-items-start col-12 col-lg-6">
                        <b-img src="{{ url('/images/icons/icon-2.jpg') }}" left blank-src class="mt-5 pt-2"></b-img>
                        <h3>Manage Your Closers With Our All-In-One Platform</h3>
                        <p class="mr-lg-5 pr-lg-4">No need for Whatsapp, Slack, or any additional software. With Closers.com you'll be able to chat with your Closers, see their bookings, the revenue they've generated for your business, and more.</p>
                        <div class="button-wrapper d-flex flex-row justify-content-between mt-4">
                            <b-button variant="primary" size="lg" href="{{ url('#') }}">Request to join</b-button>
                        </div>
                    </b-col>
                    <b-col class="col-6 d-none d-lg-flex justify-content-end">
                        <slide-animation
                            :data-step="2"
                            src="{{ url('/images/illustrations/02.png') }}"
                            direction="right"
                        ></slide-animation>
                    </b-col>
                </b-row>
                <b-row>
                    <b-col class="col-6 d-none d-lg-flex flex-row align-items-center pt-5 mt-5">
                        <slide-animation
                            :data-step="3"
                            src="{{ url('/images/illustrations/03.png') }}"
                            direction="left"
                        ></slide-animation>
                    </b-col>
                    <b-col class="d-flex flex-column align-items-start col-12 col-lg-6">
                        <h3>How Closers.com Ensures 100% Success Rate</h3>
                        <p class="mr-lg-4 pr-lg-4">The goal for Closers.com is to have 100% success rate for all our partners. That's why Closers.com is a closed-platform and only accepts new partners after a strict application process.</p>
                        <p class="mt-4 mr-lg-1 pr-lg-1">All stats will be tracked diligently - including, but no limited to: calls made, calls closed, closing ratio, revenue generated, This will ensure we are scaling your business toward your goals</p>
                        <div class="button-wrapper d-flex flex-row justify-content-between mt-5 pt-3">
                            <b-button variant="primary" size="lg" href="{{ url('#') }}">Request to join</b-button>
                        </div>
                    </b-col>
                </b-row>
                <b-row>
                    <b-col class="text-center">
                        <h3>How it works</h3>
                    </b-col>
                </b-row>
                <b-row class="pr-4 pl-4 slide-up">
                    <b-col class="pr-4 pl-4 col-12 col-lg">
                        <fade-animation
                            :data-step="1"
                            start="1"
                        >
                            <b-img src="{{ url('/images/illustrations/signUp.png') }}" fluid></b-img>
                            <h4 class="mt-2 mb-4">Request to join</h4>
                            <p>
                                Once you've completed the application, our team will review to ensure it's a good fit for both parties. If approved, our team will reach out to you.
                            </p>
                        </fade-animation>
                    </b-col>
                    <b-col class="pr-4 pr-lg-5 pl-4 pl-lg-5 col-12 col-lg">
                        <fade-animation
                            :data-step="1"
                            start="2"
                        >
                            <b-img src="{{ url('/images/illustrations/matchClosers.png') }}" fluid></b-img>
                            <h4 class="mt-2 mb-4">Match with Closers</h4>
                            <p>
                                Our team will recommend High-Ticket Closers&trade; which we believe are the best fit for your needs. However, you can choose any Closer from our pool.
                            </p>
                        </fade-animation>
                    </b-col>
                    <b-col class="pr-4 pl-4 col-12 col-lg">
                        <fade-animation
                            :data-step="1"
                            start="3"
                        >
                            <b-img src="{{ url('/images/illustrations/sales.png') }}" fluid></b-img>
                            <h4 class="mt-2 mb-4">Start Closing High-Ticket Offers</h4>
                            <p>
                                Once you have found your High-Ticket Closers&trade;, they will start closing deals for you.
                            </p>
                        </fade-animation>
                    </b-col>    
                </b-row>
                <b-row>
                    <b-col class="d-flex align-items-center justify-content-center">
                        <b-button variant="primary" size="lg" href="{{ url('#') }}">Request to join</b-button>
                    </b-col>
                </b-row>
                <b-row>
                    <b-col>
                        <div class="banner mt-4 mb-4 mt-lg mb-lg p-3 p-md-5">
                            <b-row>
                                <b-col>
                                    <h3>Join closers now to grow your sales team</h3>
                                </b-col>
                                <b-col md="auto" class="d-flex align-items-center">
                                    <b-button variant="primary" text="primary" size="lg" href="{{ url('#') }}">Request to join</b-button>
                                </b-col>
                            </b-row>
                        </div>
                    </b-col>
                </b-row>
            </b-container>
            @include('common.footer')
        </div>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>