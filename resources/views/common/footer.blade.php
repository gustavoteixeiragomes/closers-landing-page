<div id="footer">
    <b-container>
        <b-img-lazy alt="Closers.com" title="Closers.com" src="{{ url('/images/logo/closers_logo_grayscale.png') }}"></b-img-lazy>
        <b-row class="mt-5">
            <b-col class="col-6 col-lg">
                <h5>How it works</h5>
                <ul>
                    <li><b-link href="{{ url('#') }}">Closers</b-link></li>
                    <li><b-link href="{{ url('#') }}">Companies</b-link></li>
                    <li><b-link href="{{ url('#') }}">Marketplace</b-link></li>
                </ul>
            </b-col>
            <b-col class="col-6 col-lg">
                <h5>About us</h5>
                <ul>
                    <li><b-link href="{{ url('#') }}">Company</b-link></li>
                    <li><b-link href="{{ url('#') }}">Contact us</b-link></li>
                </ul>
            </b-col>
            <b-col class="col-6 col-lg">
                <h5>News</h5>
                <ul>
                    <li><b-link href="{{ url('#') }}">News</b-link></li>
                    <li><b-link href="{{ url('#') }}">Press</b-link></li>
                </ul>
            </b-col>
            <b-col class="col-6 col-lg">
                <h5>Legal</h5>
                <ul>
                    <li><b-link href="{{ url('#') }}">Privacy Policy</b-link></li>
                    <li><b-link href="{{ url('#') }}">Terms & Conditions</b-link></li>
                </ul>
            </b-col>
            <b-col class="col-6 col-lg">
                <h5>Follow us</h5>
                <ul>
                    <li>
                        <b-link href="https://www.facebook.com/closerscom/"><b-img-lazy src="{{ url('/images/icons/Fb.svg') }}" class="mr-2"></b-img-lazy> Facebook</b-link>
                    </li>
                    <li>
                        <b-link href="https://ca.linkedin.com/company/closerscom"><b-img-lazy src="{{ url('/images/icons/Linkedin.svg') }}" class="mr-2"></b-img-lazy> LinkedIn</b-link>
                    </li>
                    <li>
                        <b-link href="https://angel.co/closerscom#"><b-img-lazy src="{{ url('/images/icons/Angelist.svg') }}" class="mr-2"></b-img-lazy> Angelist</b-link>
                    </li>
                </ul>
            </b-col>
            <b-col class="col-6 col-lg">
                <h5>Contact</h5>
                <ul>
                    <li><b-link href="mailto:info@closers.com">info@closers.com</b-link></li>
                    <li>
                        <b-link href="{{ url('#') }}">
                            Vancouver, BC
                            <br />
                            Canada
                        </b-link>
                    </li>
                </ul>
            </b-col>
        </b-row>
        <span id="choose_language" class="float-left">
            <b-img-lazy src="{{ url('/images/icons/flag-usa.png') }}"></b-img-lazy>
            &nbsp;
            English (United States)
        </span>
        <span id="copyright" class="float-right">closers.com 2018</span>
    </b-container>
</div>