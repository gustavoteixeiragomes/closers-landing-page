<b-container id="header">
    <b-navbar toggleable="lg">
        <b-navbar-brand href="{{ url('#') }}">
            <b-img alt="Closers.com" title="Closers.com" src="{{ url('/images/logo/closers_logo.png') }}"></b-img>
        </b-navbar-brand>

        <b-navbar-toggle target="nav_collapse"></b-navbar-toggle>

        <b-collapse is-nav id="nav_collapse">
            <b-navbar-nav class="ml-auto text-right">
                <b-nav-item href="{{ url('#') }}">Post an opportunity</b-nav-item>
                <b-nav-item href="{{ url('#') }}">Request to join</b-nav-item>
                <b-nav-item href="{{ url('#') }}">Login</b-nav-item>
            </b-navbar-nav>
        </b-collapse>
    </b-navbar>
</b-container>