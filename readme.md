## How to install/run

This project was build using the Laradock (https://laradock.io) to setup the environment
- Guide: https://laradock.io/guides/

### So first you need to clone the project and update the submodule

- Clone the project (git clone https://gitlab.com/gustavoteixeiragomes/closers-landing-page.git)
- cd closers-landing-page/laradock/
- git submodule init
- git submodule update
- cp env-example .env

### Install and run the docker
- Create your laradock container: docker-compose up -d nginx mysql
- Go to your workspace: docker-compose exec workspace bash

### Install Laravel Dependencies
- composer install
- Copy env file: cp .env.example .env
- php artisan key:generate
- exit
- cd ..
- sudo chmod -R 777 storage bootstrap/cache

### See the Landing page
- Open the localhost (http://localhost/)